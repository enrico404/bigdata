import nltk
import pandas as pd
import sklearn



def passengers_features(passenger):
    features = {}
    features['Sex'] = passenger.Sex
    features['Age'] = passenger.Age
    features['Pclass'] = passenger.Pclass
   # features['Fare'] = passenger.Fare

    return features


titanic = pd.read_csv('./titanic/train.csv')
# effettuo uno shuffle randomico dei dati, random_state è il seme che fisso
titanic = sklearn.utils.shuffle(titanic, random_state=2210)
titanic = titanic.reset_index()
titanic.drop(columns=['index'], inplace=True)


# il trainset contiene l'80% dei valori
trainData = titanic[179:]
trainSet = []
for passenger in trainData.itertuples():
    features = passengers_features(passenger)
    trainSet.append((features, passenger.Survived))


# il test set è creato a partire dal 20% dei dati
testData = titanic[0:178]
testSet = []
for passenger in testData.itertuples():
    features = passengers_features(passenger)
    testSet.append((features, passenger.Survived))


# Train and classify
classifier = nltk.NaiveBayesClassifier.train(trainSet)

print("accuratezza: ",nltk.classify.accuracy(classifier, testSet))
classifier.show_most_informative_features(10)



# {'Sex': 'female', 'Age': 38.0, 'Pclass': 1}
#
# class Passenger():
#     def __init__(self, sex, age, pclass):
#         self.Sex = sex
#         self.Age = age
#         self.Pclass = pclass
#
# p1 = Passenger('female', 38, 1)
#
# print(classifier.classify(passengers_features(p1)))
#
# p2 = Passenger('male', 22, 1)
# print(classifier.classify(passengers_features(p2)))