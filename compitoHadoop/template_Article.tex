\documentclass[]{article}
\usepackage[italian]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage{listings}
\usepackage{color}
%opening
\title{Approfondimento su Hadoop}
\author{Sedoni Enrico}

\begin{document}

\maketitle

\begin{abstract}
Il seguente articolo tratta approfonditamente dell'architettura interna di Hadoop e del suo funzionamento. Vengono in particolare trattati argomenti come: proprietà principali di Hadoop, HDFS filesystem, YARN e MapReduce. È presente inoltre un focus su come garantire l'affidabilità del sistema nel single point of failure di Hadoop. L'articolo tratta infine anche delle politiche di scheduling che adotta Hadoop e quale è consigliata di scegliere in base alla situazione.
\end{abstract}

\tableofcontents
\newpage
\section{Cos'è Hadoop}

Hadoop è un framework open source, creato nel 2005, per la gestione di applicazioni distribuite con frequente accesso ai dati. Hadoop quindi è in grado di gestire i cosidetti "BigData" ovvero una grande mole di dati in maniera efficiente e sicura. Hadoop è utilizzato da grande aziende come Yahoo, Facebook e Linkendin.

\section{Struttura}

Hadoop tipicamente viene installato su un cluster di macchine, ogni macchina viene denominata "commodity" e lavorano tutte insieme per creare un unico sistema. I client si connetteranno da remoto al cluster Hadoop e sottometteranno i job da eseguire (fig. \ref{fig:clusterhadoop} )


\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{clusterHadoop}
	\caption[]{Cluster Hadoop}
	\label{fig:clusterhadoop}
\end{figure}

\section{Proprietà principali del sistema}

Alcune proprietà principali del sistema Hadoop sono:

\begin{itemize}
	\item \textit{potenza di calcolo:} maggiori sono i nodi computazionali, maggiore sarà la potenza;
	\item \textit{fault tollerance:} protezione dai fallimenti hardware e dati ridondanti;
	\item \textit{flessibilità:} possibile memorizzazione di qualsiasi tipologia di file;
	\item \textit{basso costo:} il framework è open-source ed è possibile anche utilizzare hardware economico per archiviare un grande volume di dati;
	\item \textit{scalabilità:} possibilità di aggiungere facilmente più nodi al cluster.
\end{itemize}


\section{Architettura}

Hadoop presenta un'architettura distribuita di tipo \textit{master/slave}, le componenti principali sono:

\begin{itemize}
	\item \textit{Hadoop Common:} strato software di supporto agli altri moduli;
	\item \textit{Hadoop Distributed File System (HDFS):} file system distribuito presente nelle \textit{commodity machine}, che si occupa della memorizzazione dei dati;
	\item \textit{Yet Another Resource Negotiator (YARN):} è il resource manager, è stato introdotto da Hadoop 2, si occupa della gestione delle risorse (memoria/cpu/storage);
	\item \textit{Map/Reduce:} modello di programmazione per il processing di grandi mole di dati.

\end{itemize}



\subsection{Hadoop Distributed File System (HDFS)} 
È un file system distribuito ispirato al Google File System (GFS) ed è ottimizzato per lavorare con grandi quantità di dati. Replica i dati, in questo modo è in grado di gestire i fallimenti dei nodi.

\setlength\parindent{0pt}
\vspace{1cm}
HDFS ha un architettura \textit{master/slave}, le componenti principali sono:
\begin{itemize}
	\item \textit{NameNode}: è il master che dirige gli slave nelle operazioni di I/O a basso livello. Tiene traccia di come i file sono divisi a blocchi e in quali \textit{DataNode} sono memorizzati;
	\item \textit{DataNode}: sono gli slave e gestiscono i dati veri e propri relativi al proprio nodo, eseguono effettivamente le operazioni sui blocchi, come la creazione, la cancellazione e la replica sotto le istruzioni del master.
\end{itemize}

\subsubsection{Blocchi}
Un file in un filesystem HDFS viene tipicamente diviso in blocchi di dati. La dimensione standard è di 64MB per blocco, aumentata a 128MB nella seconda versione di Hadoop (la versione è aumentabile a piacere). È la più piccola unità nel file system, che viene creata, duplicata o eliminata dal \textit{NameNode}.


I blocchi vengono replicati e memorizzati in maniera distribuita nel cluster in diversi modi, in questo modo si garantisce la falult tollerance e riduce il rischio di guarsti catastrofici, anche in caso di fallimento di numerosi nodi.


\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{ArchHDFS}
	\caption{Architettura HDFS}
	\label{fig:archhdfs}
\end{figure}

\subsection{MapReduce}
MapReduce è un framework per la creazione di applicazioni in grado di elaborare grandi quantità di dati in maniera parallela basandosi sul concetto di \textit{functional programming}. I dati non sono quindi condivisi  e sono passati tra le funzioni come parametri o valori di ritorno.

\vspace{1cm}

MapReduce segue il principio di \textit{divide et impera}, ovvero l'operazione di calcolo viene suddivisa e processata in diverse parti e poi, attraverso una funzione di riduzione, i risultati parziali vengono ricomposti in un unico risultato finale.
Il framework fa uso dei \textit{compute node} ovvero dei nodi di elaborazione che si trovano assieme ai \textit{data node} di HDFS. I due componenti lavorano quindi in congiunzione. 

\subsubsection{Funzionamento}
Un job map reduce tipicamente è formato da 4 componenti (fig. \ref{fig:mapreduce}):

\begin{itemize}
	\item dati di input (su HDFS);
	\item funzione Map che "spezza" l'input in diverse parti (serie di coppie chiave/valore);
	\item funzione Reduce che elabora i valori e crea come output una o più coppie chiave/valore ;
	\item output scritto su un file HDFS.
\end{itemize}

\begin{figure} 
	\centering
	\includegraphics[width=\linewidth]{mapReduce}
	\caption{Job MapReduce}
	\label{fig:mapreduce}
\end{figure}

\subsubsection{Architettura}

MapReduce a livello architetturale presenta due componenti e segue il principio \textit{master/slave} (fig. \ref{fig:mapreduce2} ):

\begin{itemize}
	\item \textit{Job Tracker}: è il master, si occupa della distribuzione del lavoro, della gestione delle risorse e del ciclo di vita dei job MapReduce. Esiste un solo job tracker per cluster. È tipicamente eseguito sul NameNode (HDFS);
	\item \textit{Task Tracker}: sono le componenti presenti nei singoli nodi ed eseguono i task sotto la direzione del job tracker. Sono le componenti slave.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{mapReduce2}
	\caption{ Architettura MapReduce}
	\label{fig:mapreduce2}
\end{figure}



\subsection{YARN}

Con Hadoop 2.0 è stato instrodotto YARN, che è un framework per la creazione di applicazioni o infrastrutture di calcolo distribuito. La seconda versione di MapReduce è stata riscritta come applicazione YARN, con alcune differenze. Le funzioni svolte dal JobTracker (MapReduce) sono affidate a due componenti:

\begin{itemize}
	\item \textit{ResourceManager}: si occupa dell'assegnazione delle risorse alle applicazioni;
	\item \textit{ApplicationMaster}: gestisce la singola applicazione, la fa partire, la monitora e la riavvia in caso di bisogno.
\end{itemize}

C'è anche un \textit{NodeManager}, che è un processo slave eseguito su ogni nodo del cluster, che si occupa di gestire i processi del nodo.

Un job MapReduce diventa quindi un'applicazione eseguita da un \textit{ApplicationMaster}. Il framework MapReduce diventa quindi solo uno dei tanti framework sviluppabili con YARN.


\section{Hadoop High Availability}

Uno dei maggiori problemi di Hadoop 1.0 era che il NameNode era un single point of failure (SPOF), quindi se aveva dei problemi il NameNode tutto il sistema non funzionava più ed era necessario l'intervento di un tecnico per riportare il sitema funzionante. Con Hadoop 2.0 abbiamo un nodo chiamato \textit{standby node} che in caso di fallimento del NameNode lo rimpiazza automaticamente, in modo da garantire il corretto funzionamento del sitema. Con Hadoop 3.0 (attuale) addirittura abbiamo nodi di standby multipli, in modo da diventare ancora più affidabile il sistema. 


\vspace{1cm}

La gestione automatica dei fallimenti del NameNode aggiunge una serie di componenti sottostanti nello sviluppo dell'HDFS:

\begin{itemize}
	\item ZooKeeper quorum;
	\item ZKFailoverController Process (ZKFC).
\end{itemize}


\subsubsection{ZooKeeper quorum}

È un sistema centralizzato che mantiene dati di coordinazione, configurazione e naming. Prevede una serie di servizi e la sincronizzazione. Tiene informati i client sui cambiamenti dei dati e traccia i fallimenti dei client.

Alcune funzioni che svolge \textit{ZooKeeper quorum} per rilevare e gestire i fallimenti dei NameNode sono:

\begin{itemize}
	\item \textit{Failure detection}: Zookeper mantiene una sessione con il NameNode, se questo fallisce, la sessione scade. Zookeper dovrà quindi informare gli altri NameNodes di far partire il processo di recovery;
	\item \textit{Active NameNode election}: metodo per l'elezione del prossimo NameNode come attivo.
\end{itemize}



\subsubsection{ZKFailoverController Process (ZKFC)}

\textit{ZKFC} è un demone che gira su tutti le macchine NameNode e serve a monitorare e gestire lo status di questi nodi. Si occupa di:

\begin{itemize}
	\item Monitoring dello status
	\item Mantiene aperta la sessione con Zookeper. Se il NameNode è attivo, mantiene il lock \textit{zonode}. Se la sessione scade, il lock viene cancellato.
	\item Elezione: se il demone si accorge che nessuno degli altri nodi detiene il lock di \textit{znode} e il NameNode è "in salute",  il demone tenta di acquisire il lock su \textit{znode}. Se ci riesce diventa il nuovo NameNode attivo.
\end{itemize}

\section{Hadoop Schedulers}

Un sistema Hadoop è quindi un sistema multitasking in grado di processare più dataset per più utenti simultaneamente.

Come possiamo servire più utenti per volta?

Come per i classici sistemi multitasking sono necessarie delle politiche di scheduling, al posto di schedulare i processi, si schedulano i vari jobs.

\subsubsection{Tipi di scheduler principali}
\vspace{1cm}
\textbf{FIFO Scheduler} 
\vspace{0.5cm}

È l'algoritmo di job scheduling che era integrato con il primo JobTracker. Segue la classica politica FIFO, il primo job ad entrare in coda è il primo a venire eseguito. Tuttavia non si può dare priorità ai job, quindi può non essere adatto per certi sistemi.
\vspace{1cm}

\textbf{Fair Scheduler}
\vspace{0.5cm}

Quando si hanno tanti utenti che mandano simultaneamente all'Hadoop cluster jobs da eseguire, si verifica il problema di quanto tempo di calcolo dedicare ad un determinato utente. Il Fair Scheduler sostanzialmente condivide le capacità del cluster equamente a tutti gli utenti. Va a creare degli "slots" di tempo di esecuzione che vengono dati man mano ai vari jobs. Quando un job finisce lo slot esso viene preemptato e torna in coda. Grazie a questa tecnica si riesce a dedicare un equo tempo di esecuzione a tutti i job nella coda.
\vspace{1cm}

\textbf{Capacity Scheduler}
\vspace{0.5cm}

È molto simile al Fair Scheduler ma i jobs vengono schedulati utilizzando code FIFO con priorità. I jobs possono quindi avere priorità differenti. Inoltre permette per ogni utente di simulare un cluster separato attraverso lo scheduling FIFO.

\subsubsection{Quale scheduler utilizzare?}

Per realtà grandi con accessi multipli di utenti al cluster, il Capacity Scheduler è la soluzione ideale. Per realtà più piccole o comunque con un carico di lavoro limitato anche il Fair Scheduler lavora bene, è anche più semplice da configurare.


\section{Limitazioni}

Hadoop è un sistema molto potente e scalabile per i bigdata, tuttavia ci sono diverse limitazioni che il sistema non è in grado di gestire efficientemente. Alcune limitazioni importanti sono: la gestione dei piccoli file, non gestisce i dati in tempo reale, non è efficiente per il calcolo iterativo (a causa di map/reduce) e infine non è efficiente nel caching dei dati.
\vspace{0.5cm}

A causa di tutte queste limitazioni a cui è soggetto il sistema, sono nati altri sistemi sulla base di Hadoop che vanno a risolvere la maggior parte delle limitazioni. Spark è uno di questi.





\nocite{*}
\bibliographystyle{ieeetran}
\bibliography{biblio.bib}


\end{document}



