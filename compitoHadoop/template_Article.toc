\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Cos'è Hadoop}{2}{section.1}% 
\contentsline {section}{\numberline {2}Struttura}{2}{section.2}% 
\contentsline {section}{\numberline {3}Proprietà principali del sistema}{2}{section.3}% 
\contentsline {section}{\numberline {4}Architettura}{3}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Hadoop Distributed File System (HDFS)}{3}{subsection.4.1}% 
\contentsline {subsubsection}{\numberline {4.1.1}Blocchi}{3}{subsubsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.2}MapReduce}{3}{subsection.4.2}% 
\contentsline {subsubsection}{\numberline {4.2.1}Funzionamento}{4}{subsubsection.4.2.1}% 
\contentsline {subsubsection}{\numberline {4.2.2}Architettura}{5}{subsubsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.3}YARN}{5}{subsection.4.3}% 
\contentsline {section}{\numberline {5}Hadoop High Availability}{6}{section.5}% 
\contentsline {subsubsection}{\numberline {5.0.1}ZooKeeper quorum}{6}{subsubsection.5.0.1}% 
\contentsline {subsubsection}{\numberline {5.0.2}ZKFailoverController Process (ZKFC)}{7}{subsubsection.5.0.2}% 
\contentsline {section}{\numberline {6}Hadoop Schedulers}{7}{section.6}% 
\contentsline {subsubsection}{\numberline {6.0.1}Tipi di scheduler principali}{7}{subsubsection.6.0.1}% 
\contentsline {subsubsection}{\numberline {6.0.2}Quale scheduler utilizzare?}{8}{subsubsection.6.0.2}% 
\contentsline {section}{\numberline {7}Limitazioni}{8}{section.7}% 
