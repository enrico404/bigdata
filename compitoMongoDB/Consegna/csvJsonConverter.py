
from os import listdir
from os.path import isfile, join
import pandas
from ast import literal_eval

class csvJsonConverter:
    """
    the csv to json converter use pandas as external module in order to mantain the correct datatypes
    """
    def __init__(self, path):
        self.path = path

    def convert(self, name, remake=False):
        files = [f for f in listdir(".") if isfile(join(".", f))]
        #check if the file already exists, i'll check also the remake flag
        if name+".json" not in files or remake:
            f = open(self.path, 'r')
            reader = pandas.read_csv(f)
            f.close()

            #the currency columns due to their format ($xxx.yyy) needs to be manipulated and converted from string to double
            #this array contains the names of the currency columns.
            #With the conversion we can perfom operations like: price > 100...
            colnames = ['price', 'weekly_price', 'monthly_price', 'cleaning_fee', 'extra_people', 'security_deposit', 'adjusted_price' ]

            for k, val in reader.iteritems():
                if k in colnames:
                        reader[k] = reader[k].str.replace('$', '')
                        reader[k] = reader[k].str.replace(',', '')
                        reader[k] = pandas.to_numeric(reader[k])
                # interpret the field "host_verification" as a list
                if k == 'host_verifications':
                    reader[k] = reader[k].apply(literal_eval)
            # parse the dataframe into jsonArray
            export = reader.to_json(name+'.json', orient='records')
            print("the file "+name+".json has been successfully created")
        else:
            print("the file "+name+".json already exists")