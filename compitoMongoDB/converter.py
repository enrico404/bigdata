#!/usr/bin/env python3
import csvJsonConverter
import sys
if __name__=='__main__':
    sys.argv.pop(0)
    for par in sys.argv:
        if par == "-h":
            print("SYNOPSIS: ")
            print("./converter.py filename...")
            print("DESCRIPTION:")
            print("converts a csv file to json")
        else:
            ext = par.split(".")
            if ext[1] == "csv":
                converter = csvJsonConverter.csvJsonConverter(par)
                converter.convert(ext[0])
            else:
                print("the file extension is not supported")
