from neo4jrestclient.client import GraphDatabase
from nltk.corpus import stopwords
import nltk

db = GraphDatabase(
    "http://35.175.102.204:33020",
    username="neo4j", password="length-slinging-battles")


query = '''
MATCH (m:Movie)
Return m
'''

movies = db.query(query)

for movie in movies:
    try:
        #inizializzo nuovo array keyword per il movie
        keywordsTMP = []
        keywords=[]
        #estraggo il testo dal campo plot del movie
        text = movie[0]['data']['plot']

        #ricavo le parole chiave attraverso la libreria nltk
        tokens = nltk.word_tokenize(text)
        wnl = nltk.WordNetLemmatizer()
        for t in tokens:
            if not t in stopwords.words('english') and t not in [",",".",":"]:
                keywordsTMP.append(wnl.lemmatize(t))
        keywordsTMP = nltk.pos_tag(keywordsTMP)

        for w, pt in keywordsTMP:
            if pt in ["NN", "NNP", "NNS","JJ"]:
                keywords.append(w)

        #aggiungo la lista di keywords nel nodo movie interessato
        query = "match (movie:Movie)" \
                 " where movie.title=\""+movie[0]['data']['title']+"\"" \
               " set movie.keywords = "+str(keywords) +" return movie;"
        res = db.query(query)
    except:
        print("nodo "+movie[0]['data']['title']+" non aggiornato poichè manca campo plot")