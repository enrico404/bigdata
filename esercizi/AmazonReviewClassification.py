import nltk
import random

from nltk.sentiment.util import *
from pymongo import MongoClient

random.seed(2213)


# SENTIMENT ANALYSIS - AMAZON REVIEWS CLASSIFICATION

# Retrieves document (review) data from mongodb
# Returns: [ (list of words review1, cat review1), ...]
def getReviewData():
    client = MongoClient('localhost', 27018)
    client.test.authenticate('studenti', '20studenti17 ', mechanism='SCRAM-SHA-1')
    db = client.test
    reviews = []
    for review in db.reviews.find({'overall':5.0}).limit(1000):
        reviews.append((nltk.word_tokenize(review["reviewText"]),'pos'))
    for review in db.reviews.find({'overall': 1.0}).limit(1000):
        reviews.append((nltk.word_tokenize(review["reviewText"]), 'neg'))

    return reviews


# Create train and test sets
reviews = getReviewData()
random.shuffle(reviews)
sentimAnalyzer = nltk.sentiment.SentimentAnalyzer()
allWordsNeg = sentimAnalyzer.all_words([mark_negation(doc) for doc in reviews])
#estraggo feature di singole parole, è possibile specificare la frequenza minima di una parola
unigramFeats = sentimAnalyzer.unigram_word_feats(allWordsNeg, min_freq=4)
sentimAnalyzer.add_feat_extractor(extract_unigram_feats, unigrams=unigramFeats)
featureSets = sentimAnalyzer.apply_features(reviews)
trainSet, testSet = featureSets[100:], featureSets[:100]

# Train and classify
classifier = sentimAnalyzer.train(nltk.classify.NaiveBayesClassifier.train, trainSet)
for key, value in sorted(sentimAnalyzer.evaluate(testSet).items()):
    print(key, " : ", value)