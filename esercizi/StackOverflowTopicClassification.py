import nltk
import random
import sys

from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client

#reload(sys)  # only for python2
#sys.setdefaultencoding('utf8')  # only for python2

random.seed(2213)


# TOPIC CLASSIFICATION - NEO4J STACK OVERFLOW POSTS

# Retrieves document (post) data from neo4j
# Returns: [ (list of words doc1, cat doc1), ...]
def getDocsData():
    db = GraphDatabase("http://localhost:7474", username="neo4jstudent", password="20marzo17")
    q = ''' match (p:Post)-[:HAS_TAG]->(t:Tag)
            where t.tagId = 'neo4j' 
            return p.title, t.tagId limit 1000  
            
            UNION
            
            match (p:Post)-[:HAS_TAG]->(t:Tag)
            where t.tagId = 'nltk' 
            return p.title, t.tagId limit 1000  
            
        '''
    results = db.query(q, returns=(str, str))
    return [(nltk.word_tokenize(r[0]), r[1]) for r in results]


# Returns the features (dictionary) for a given document
def docFeatures(document):
    docWords = set(document)
    features = {}
    for word in vocabulary:
        features["contains("+word+")"] = (word in docWords)
    return features


# Create train and test sets
docs = getDocsData()
random.shuffle(docs)
allWords = [word.lower() for doc in docs for word in doc[0]]
vocabulary = [word for word, count in nltk.FreqDist(allWords).most_common(3000)]
featureSets = [(docFeatures(d), c) for (d, c) in docs]
trainSet, testSet = featureSets[100:], featureSets[:100]

# Train and classify
classifier = nltk.NaiveBayesClassifier.train(trainSet)
print(nltk.classify.accuracy(classifier, testSet))
classifier.show_most_informative_features(10)