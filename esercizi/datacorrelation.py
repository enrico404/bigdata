import pandas as pd
import scipy.stats as sci
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
data = pd.read_csv('data_visualization.csv', index_col=0)
corr1 = data.corr(method="pearson", min_periods=1)
print("----------Pearson Correlation-------------")
print(corr1)


print("--------------")
corr, p = sci.pearsonr(data['rho'], data['wind'])
print("valore di corr: ", corr, " valore di p: ", p)


print("-----data visualization------")


# Load the example flights dataset and conver to long-form


sns.set()
ax = sns.heatmap(corr1)
plt.show()



