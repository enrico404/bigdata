import nltk
import random

random.seed(2210) #modificato da 2213 per funzionare correttamente in python3


# GENDER CLASSIFICATION

# Returns the features (dictionary) for a given name
#devo ritornare le feature, perchè è attraverso queste che si classifica
def gender_features(name):
    #features = {}
    #features["first_letter"] = name[0].lower()
    #features["last_letter"] = name[-1].lower()
    #for letter in 'abcdefghijklmnopqrstuvwxyz':
     #   features["count({})".format(letter)] = name.lower().count(letter)
      #  features["has({})".format(letter)] = (letter in name.lower())
    return {'suffix':name[0], 'suffix1': name[-1], 'suffix2': name[-2]}


# Create train, test and devtest sets
labeled_names = ([(name, 'male') for name in nltk.corpus.names.words('male.txt')] + [(name, 'female') for name in
                                                                                     nltk.corpus.names.words(
                                                                                         'female.txt')])
random.shuffle(labeled_names)
# 79/80% nomi di sample
train_names = labeled_names[1500:]
devtest_names = labeled_names[500:1500]
#primi 500 nomi per il testing
test_names = labeled_names[:500]
#set di training, array delle feature dei nomi, è quello che prende in pasto il classificato
train_set = [(gender_features(n), gender) for (n, gender) in train_names]
devtest_set = [(gender_features(n), gender) for (n, gender) in devtest_names]
#set di test
test_set = [(gender_features(n), gender) for (n, gender) in test_names]

# Train and classify
classifier = nltk.NaiveBayesClassifier.train(train_set)



print('Neo è:'+classifier.classify(gender_features('Neo')))
print('Trinity è:'+classifier.classify(gender_features('Trinity')))
print("accuratezza: ",nltk.classify.accuracy(classifier, test_set))
# output xx : 1.0  -> x volte a 1
classifier.show_most_informative_features(10)


#provo a vedere quali errori vengono fatti
errors = list()
for (name,tag) in devtest_names:
    guess = classifier.classify(gender_features(name))
    if guess != tag:
        errors.append((tag, guess, name))

# for (tag, guess, name) in sorted(errors):
#     print('correct={:<8} guess:{:<30} name={:<30}'.format(tag,guess,name))