import pandas as pd

df = pd.read_csv('tidydata.csv')

tidyDf = pd.melt(df, ['religion'], var_name='income', value_name='freq').sort_values(by='religion')

print(tidyDf)