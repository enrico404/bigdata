import pandas as pd
from numpy import *

df = pd.read_csv("http://rcs.bu.edu/examples/python/data_analysis/Salaries.csv")
print(df.size)
print(df.columns)
print(df.dtypes)
print(df.describe())
print("Standard deviation:")
print(df.std())
print("Mean values of the first 50 records:")
print(df.head(50).mean())
df_salary=df[['salary']]
print("Statistics of the salary column:")
print(df_salary.describe())
print("Number of salary values:")
print(df_salary.count())
print("Avg. salary:")
print(df_salary.mean())
df_female=df[df['sex'] == 'Female']
df_fs=df_female[['salary']]
#
# #equivalent to: df_fs=df[df['sex'] == 'Female'][['salary']]
#
print("Salary of female professors:")
print(df_fs)
var = (df_salary - df_salary.mean())/df_salary.std()
print(var)
#map example
def dollarConvert(x):
    return x*1.14

df['dollar']=df['salary'].map(dollarConvert)
print(df)
#
# #Series str attribute
# print(df['sex'].str.lower())
#
#Group data using rank
df_rank = df.groupby(['rank'])
print(df_rank[['salary']].describe())

print(df[df['rank']=='Prof']['salary'])
#
# #Calculate mean value for each numeric column per each group
# print(df_rank.mean())
#
#concat

df1=pd.DataFrame({'rank':['AssocProf','Prof','AsstProf'],'dollar':[91786.23,123624.80,81362.78]})
print(df1)
df_u=pd.concat([df,df1])
print(df_u)
# print(df.append(df1))
#
# merge

df2=pd.DataFrame({'discipline':['A','B'], 'description':['computer science','math']})
df_d=pd.merge(df,df2)
# #df_d=pd.merge(df,df2,left_index=True,right_index=True)
print(df_d)




